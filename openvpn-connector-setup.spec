%define build_target %{_vendor}_nover
%if 0%{?suse_version}
%define build_target suse%{suse_version}
%endif
%if 0%{?fedora}
%define build_target fc%{?fedora}
%endif
%if 0%{?rhel}
%define build_target rhel%{?rhel}
%endif

# %%define buildrev 20230504gitcf8d143

Name:           openvpn-connector-setup
Version:        6
Release:        1%{?dist}
Summary:        OpenVPN Connector setup utility for CloudConnexa™

License:        AGPLv3
URL:            https://openvpn.net/cloud-beta/
Source0:        https://swupdate.openvpn.net/community/releases/openvpn-connector-setup-%{version}.tar.gz
Source1:        https://swupdate.openvpn.net/community/releases/openvpn-connector-setup-%{version}.tar.gz.asc
Source2:        gpgkey-F554A3687412CFFEBDEFE0A312F5F7B42F2B01E7.gpg
Vendor:         OpenVPN Inc

BuildArch:      noarch
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  gnupg2
BuildRequires:  make

%description
This utility helps configure this host as an OpenVPN Connector
in a CloudConnexa™ environment

%package -n python3-%{name}
Summary:        OpenVPN Connector setup utility for CloudConnexa™
Requires:       openvpn3-client
Requires:       python(abi) >= 3.6

%if 0%{?rhel} < 8 && 0%{?fedora} < 30
Requires:       python36-cryptography
Requires:       python36-dbus
%else
Requires:       python3-cryptography
Requires:       python3-dbus
%endif

%description -n python3-%{name}
Configuration utility to configure the
host to connect to the CloudConnexa™ service.

%package -n cockpit-openvpn-connector
Summary:      CloudConnexa™ setup module for Cockpit
Requires:     cockpit
Requires:     python3-openvpn-connector-setup >= %{version}

%description -n cockpit-openvpn-connector
This adds a Cockpit module to configure and manage
CloudConnexa™ profiles and VPN sessions.


%prep
gpgv2 --quiet --keyring %{SOURCE2} %{SOURCE1} %{SOURCE0}
%autosetup -n %{name}-%{version}

%build
%py3_build

%install
%py3_install
%if 0%{?rhel} < 8
find %{buildroot}/%{python3_sitelib}/openvpn_connector_setup-*.egg-info/ -type f -name requires.txt | xargs rm -vf
%endif

# Install cockpit-openvpn-connector
make -C cockpit-openvpn-connector DESTDIR=%{buildroot} PREFIX=%{_prefix} localstatedir=%{_localstatedir} sharedstatedir=%{_sharedstatedir} install
mkdir -m755 -p %{buildroot}/%{_datadir}/licenses/cockpit-openvpn-connector/ %{buildroot}/%{_docdir}/cockpit-openvpn-connector/

# We don't want to ship the old PolicyKit ini-style policy
rm -rf %{buildroot}/%{_localstatedir}/lib/polkit-1


%pre -n cockpit-openvpn-connector
# Ensure we have connector group account
getent group connector &>/dev/null || groupadd -r connector

exit 0


%files -n python3-%{name}
%license COPYRIGHT.md
%doc README.md
%{python3_sitelib}/openvpn_connector_setup-*.egg-info/
%{python3_sitelib}/openvpn/
%{_bindir}/openvpn-connector-setup

%files -n cockpit-openvpn-connector
%license cockpit-openvpn-connector/COPYRIGHT.txt
%doc cockpit-openvpn-connector/VERSION
%{_datadir}/cockpit/openvpn-connector/
%{_datadir}/polkit-1/rules.d/cockpit-openvpn-connector.rules

%changelog
* Tue May 23 2023 David Sommerseth <davids@openvpn.net> - 6-1
- Update to openvpn-connector-setup v6

* Tue May 9 2023 David Sommerseth <davids@openvpn.net> - 5-1
- Update to openvpn-connector-setup v5

* Mon Jan 16 2023 David Sommerseth <davids@openvpn.net> - 4-1
- Update to openvpn-connector-setup v4
- Adds Cockpit support via cockpit-openvpn-connector sub-package

* Mon May 23 2022 David Sommerseth <davids@openvpn.net> - 3-1
- Update to openvpn-connector-3

* Tue Oct 26 2021 David Sommerseth <davids@openvpn.net> - 2-1
- Update to openvpn-connector-setup-2
- Added PGP verification of the source code

* Tue Apr 14 2020 David Sommerseth <davids@openvpn.net> - 1-2
- Fixed incorrect python3-cryptography/python36-cryptography dependency
- Added missing python3-dbus dependency

* Wed Apr 8 2020 David Sommerseth <davids@openvpn.net> - 1-1
- Initial release

